import re

DEFAULT_MAX_ACRONYM_LEN = 6
DEFAULT_MAX_COMPONENT_LEN = 2
DEFAULT_MAX_NUM_OF_NE_COMPONENTS = 6
MIN_ACRONYM_LEN = 2

REGEX_SPACE_HYPHEN_SEPARATED_COMPONENT = re.compile('\w+[ \-]\w+')
EXTRACT_DOMAIN_REGEX = r'[\w\-]+\.[\w\-]+(\.[\w\-]+)*'

TO_EN_ALPHABET_DICT = {
    "ą": "a",
    "ć": "c",
    "ę": "e",
    "ł": "l",
    "ń": "n",
    "ś": "s",
    "ó": "o",
    "ź": "z",
    "ż": "z"
}

