import re

from enumeration import MIN_ACRONYM_LEN, DEFAULT_MAX_COMPONENT_LEN, DEFAULT_MAX_ACRONYM_LEN, TO_EN_ALPHABET_DICT


def normalise_acronyms(detected_acronyms_raw):
    return set(remove_nonenglish_letters(acronym.lower()) for acronym in detected_acronyms_raw)


def generate_acronyms_from_named_entities(detected_ne):
    return set(remove_nonenglish_letters(acronym) for ne in detected_ne for acronym in
               _generate_acronyms_for_named_entity(ne, 2))


def remove_nonenglish_letters(word):
    for k, v in TO_EN_ALPHABET_DICT.items():
        word = word.lower().replace(k, v)
    return word


def _generate_acronyms_for_named_entity(named_entity, max_component_len=DEFAULT_MAX_COMPONENT_LEN,
                                        min_acronym_len=MIN_ACRONYM_LEN, max_acronym_len=DEFAULT_MAX_ACRONYM_LEN):
    words_in_entity = [re.sub(r'\W+', '', word) for word in re.split('[ \-,]+', named_entity)]
    acronym_components = [_generate_acronym_components(word, max_component_len) for word in words_in_entity if word]
    return set(acronym.lower() for acronym in _build_acronyms(acronym_components) if
               min_acronym_len <= len(acronym) <= max_acronym_len)


def _generate_acronym_components(word, max_component_len):
    return [word[:x] for x in range(min(len(word), max_component_len) + 1)]


def _build_acronyms(acronym_components, acc=['']):
    # All combinations of acronym components
    if len(acronym_components) == 0:
        return []
    current_components = acronym_components[0]
    acronyms = [abb + component for abb in acc for component in current_components]
    if len(acronym_components) == 1:
        return acronyms
    else:
        return _build_acronyms(acronym_components[1:], acronyms)
