import re

from enumeration import EXTRACT_DOMAIN_REGEX
from enumeration.dns_enumeration import dict_enumeration
from scrapping.crawler import crawl_urls_for_ne_custom, crawl_website_for_ne_custom
from util.io_util import newline_separated_file_to_list


def generate_dictionary(urls_path, root_domain, depth, limit):
    if urls_path is not None:
        urls = newline_separated_file_to_list(urls_path)
        return crawl_urls_for_ne_custom(urls, root_domain)
    else:
        return crawl_website_for_ne_custom(root_domain, root_domain, depth, limit)


def dictionary_dns_enumeration(dictionary_path, domains_to_check, name_servers):
    dictionary_detected_domains = set()
    # Additional dictionary enumeration
    if dictionary_path is not None:
        print('Performing dictionary enumeration against file: ' + dictionary_path)
        with open(dictionary_path, "r") as dictionary_file:
            dictionary = set(word.rstrip() for word in dictionary_file.readlines())
            for domain in domains_to_check:
                print('Dictionary enumeration for: ' + domain)
                tmp_detected = dict_enumeration(domain, dictionary, name_servers)
                tmp_detected = set(filter(lambda detected: detected != domain, tmp_detected))
                dictionary_detected_domains.update(tmp_detected)
                print(tmp_detected)
    return dictionary_detected_domains


def extract_domains_from_links(detected_urls):
    return set(domain.group(0) for domain in (re.search(EXTRACT_DOMAIN_REGEX, url) for url in detected_urls) if
               domain is not None)
