import re
from concurrent.futures import ThreadPoolExecutor, as_completed

import dnsq
import iterutils as iterutils
from tqdm import tqdm

from enumeration import EXTRACT_DOMAIN_REGEX


def dict_enumeration(domain, dictionary, name_servers):
    result = set()
    wildcard_record_result = dnsq.query_dns('*.' + domain, 'A')
    if len(wildcard_record_result) > 0:
        return {'*.' + domain}
    with ThreadPoolExecutor() as executor:
        future_to_result = [executor.submit(_query_domain, word + '.' + domain, ns) for (word, ns) in
                            zip(dictionary, iterutils.cycle(name_servers))]
        for future in tqdm(as_completed(future_to_result), total=len(future_to_result)):
            res = future.result()
            result.update(res)
    return set(x.replace('www.', '') for x in result)


def _query_domain(domain_to_check, ns):
    result = set()
    a_result = dnsq.query_dns(domain_to_check, 'A', ns)
    cname_result = dnsq.query_dns(domain_to_check, 'CNAME', ns)
    if len(a_result) > 0:
        result.add(domain_to_check)
    if len(cname_result) > 0:
        result.update(
            set(domain.group(0) for domain in (re.search(EXTRACT_DOMAIN_REGEX, url) for url in cname_result) if
                domain is not None))
    return result
