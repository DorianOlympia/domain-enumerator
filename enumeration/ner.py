import re

from enumeration import REGEX_SPACE_HYPHEN_SEPARATED_COMPONENT, DEFAULT_MAX_ACRONYM_LEN, \
    DEFAULT_MAX_NUM_OF_NE_COMPONENTS, MIN_ACRONYM_LEN


def extract_ne_custom(source, max_num_of_ne_components=DEFAULT_MAX_NUM_OF_NE_COMPONENTS):
    # Detect all sequences of at least two words, where each starts with capital letter and words are separated with
    # space, hyphen or and (in polish i.e. 'i')
    result = _find_potential_ne(source)
    result = _generate_subnamed_entities(result)
    result = _filter_length(max_num_of_ne_components, result)
    return set(result)


def extract_all_acronyms_custom(source, max_acronym_len=DEFAULT_MAX_ACRONYM_LEN):
    # Detect all sequences of capital letters, followed by whitespace or dot and preceded with whitespace
    result = set(re.findall('(?<=\s)[A-ZĄŚŻŹÓŁĆ]{' + str(MIN_ACRONYM_LEN) + ',}(?=[\s.,])', source))
    return set(item for item in result if len(item) <= max_acronym_len)


def _generate_subnamed_entities(result):
    # For A,B,C subnamed entities are A B C AB and BC
    return [ne for item in result for ne in _generate_comma_separated_ne_combinations(re.split(',\s+', item))]


def _find_potential_ne(source):
    potential_ne = re.findall('[A-Z][a-ząężźśćół]+(?:(?:[ \-]| i |, )[A-Z][a-ząężźśćół]+)+', source)
    # Accept only string with at least one space separated or hyphen pair of words (to filter out things
    # like A,B - C which are hardly any ne)
    return set(item for item in potential_ne if REGEX_SPACE_HYPHEN_SEPARATED_COMPONENT.search(item))

def _filter_length(max_num_of_ne_components, result):
    return [item for item in result if 1 < len(re.split('[ \-,]+', item)) <= max_num_of_ne_components]


def _generate_comma_separated_ne_combinations(source):
    result = [", ".join(source[position:position + size]) for size in range(1, len(source) + 1) for position in
              range(0, len(source) + 1 - size)]
    return result
