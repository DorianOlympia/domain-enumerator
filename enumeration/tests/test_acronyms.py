from enumeration.acronyms import _generate_acronyms_for_named_entity, remove_nonenglish_letters


def test_generate_acronyms_for_named_entity():
    # given
    ne = "Akademia Górniczo-Hutnicza"
    expected = {'ag', 'gh', 'ah', 'agh'}

    # when
    result = _generate_acronyms_for_named_entity(ne, 1)

    # then
    assert expected == result


def test_generate_acronyms_for_named_entity_with_component_size_2():
    # given
    ne = "Abcd Efgh-Ijkl"
    expected = {'abefi', 'abi', 'ij', 'aef', 'abefij', 'ei', 'aeij', 'aefij', 'ef', 'eij', 'abeij', 'ab', 'ae', 'abe'
        , 'aefi', 'efij', 'abei', 'ai', 'abij', 'aei', 'efi', 'abef', 'aij'}
    # AG GH AH AGH AK GO HU AKG AKH
    # when
    result = _generate_acronyms_for_named_entity(ne, 2)

    # then
    assert expected == result


def test_generate_acronyms_for_named_entity2():
    # given
    ne = "Prusy, Rosję i Austrię"
    expected = {'pr', 'pi', 'pa', 'ri', 'ra', 'ia', 'pri', 'pra', 'pia', 'ria', 'pria'}

    # when
    result = _generate_acronyms_for_named_entity(ne, 1)

    # then
    assert expected == result


def test_remove_nonenglish_letters():
    # given
    word = "ąĘłńśćóżźł"
    expected = "aelnscozzl"

    # when
    result = remove_nonenglish_letters(word)

    # then
    assert expected == result
