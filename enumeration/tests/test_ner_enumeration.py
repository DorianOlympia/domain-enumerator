from enumeration.ner import extract_ne_custom, extract_all_acronyms_custom


def test_extract_all_ne_custom():
    # given
    expected_ne = {'Królestwo Polskie', 'Wielkim Księstwem Litewskim',
                   'Rzeczpospolita Obojga Narodów', 'Prusy, Rosję i Austrię', 'Rosję i Austrię', 'Dzeta-Teta-Jota'}

    # when
    with open('resources/ne_testfile.txt', 'r') as file:
        source = file.read()
        result = extract_ne_custom(source, 4)

    # then
    assert expected_ne == result


def test_extract_all_acronyms_custom():
    # given
    expected_ne = {'AGH', 'LOL', 'XDXD'}

    # when
    with open('resources/ne_testfile.txt', 'r') as file:
        source = file.read()
        result = extract_all_acronyms_custom(source, 4)

    # then
    assert expected_ne == result
