import argparse
import csv

from enumeration.acronyms import generate_acronyms_from_named_entities, normalise_acronyms
from enumeration.dns_enumeration import dict_enumeration
from enumeration.dns_enumeration_util import generate_dictionary, \
    dictionary_dns_enumeration, extract_domains_from_links
from util.io_util import get_result_directory, write_results, newline_separated_file_to_list


def parse_commandline_arguments():
    parser = argparse.ArgumentParser()
    parser.add_argument('root_domain', help='Domain, from which we start crawling (without protocol, in x.y.z.com form')
    parser.add_argument('output', help='Name of catalog, where results will be aggregated')
    parser.add_argument('-d', '--depth', type=int, help='Limit crawling to specified depth level', default=2)
    parser.add_argument('-D', '--dictionary',
                        help='Relative path to file containing additional \\n separated words dictionary, '
                             'used for sub-domain enumeration.')
    parser.add_argument('-ns', '--dns', help='Provide list of DNS to be used during resolution (round robin)')
    parser.add_argument('-l', '--limit', type=int, help='Limit crawling to specified amount of websites', default=50)
    parser.add_argument('-u', '--urls', help='Relative path to a file with \\n separated urls to use instead of '
                                             'crawling, options -d and -l are ignored if you specify it')
    parser.add_argument('--separate', action='store_true',
                        help='Store results from different methods in separate files')
    args = parser.parse_args()
    return args


def print_results(detected_acronyms, detected_ne, generated_acronyms):
    print('Detected NE: ' + str(len(detected_ne)))
    print('Detected acronyms: ' + str(len(detected_acronyms)))
    print('Generated acronyms: ' + str(len(generated_acronyms)))


def main():
    args = parse_commandline_arguments()

    # NE & Acronyms detection and processing
    detected_domains = set()
    detected_ne, detected_acronyms_raw, visited_urls, detected_urls = generate_dictionary(args.urls, args.root_domain,
                                                                                          args.depth,
                                                                                          args.limit)

    generated_acronyms = generate_acronyms_from_named_entities(detected_ne)
    detected_acronyms = normalise_acronyms(detected_acronyms_raw)
    write_results(detected_ne, detected_acronyms, generated_acronyms, visited_urls, args.output)
    print_results(detected_acronyms, detected_ne, generated_acronyms)

    # # Domain enumeration
    result_directory_path = get_result_directory(args.output)

    # TODO VERIFY DETECTED URLS
    gathered_domains = extract_domains_from_links(detected_urls)
    detected_domains.update(gathered_domains)
    print('Num of crawl detected domains:' + str(len(gathered_domains)))

    acronyms = set()
    acronyms.update(generated_acronyms)
    acronyms.update(detected_acronyms)

    name_servers = newline_separated_file_to_list(args.dns)
    nes_detected_domains = dict_enumeration(args.root_domain, acronyms, name_servers)
    # nes_detected_domains = normalise_dnsrecon_result(nes_detected_domains)
    detected_domains.update(nes_detected_domains)
    print('Num of NES detected domains:' + str(len(nes_detected_domains)))

    dictionary_detected_domains = dictionary_dns_enumeration(args.dictionary, nes_detected_domains, name_servers)
    detected_domains.update(dictionary_detected_domains)
    print('Num of dictionary detected domains:' + str(len(dictionary_detected_domains)))

    # Write results to file
    with open(result_directory_path + 'result', mode='w') as result_file:
        writer = csv.writer(result_file, delimiter='\n')
        writer.writerow(sorted(detected_domains))

    if (args.separate):
        with open(result_directory_path + 'result_gathered', mode='w') as result_file:
            writer = csv.writer(result_file, delimiter='\n')
            writer.writerow(sorted(gathered_domains))

        with open(result_directory_path + 'result_nes', mode='w') as result_file:
            writer = csv.writer(result_file, delimiter='\n')
            writer.writerow(sorted(nes_detected_domains))

        with open(result_directory_path + 'result_dictionary', mode='w') as result_file:
            writer = csv.writer(result_file, delimiter='\n')
            writer.writerow(sorted(dictionary_detected_domains))

    print('done!')


if __name__ == "__main__":
    main()