from util.io_util import newline_separated_file_to_list

dictionary = set(newline_separated_file_to_list('resources/20000-result'))
cewl = set(newline_separated_file_to_list('resources/cewl-result'))
search = set(newline_separated_file_to_list('resources/search-result'))
ne = set(newline_separated_file_to_list('resources/result'))

dict_cewl = dictionary.intersection(cewl)
print('dict * cewl: ' + str(len(dict_cewl)))

dict_search = dictionary.intersection(search)
print('dict * search: ' + str(len(dict_search)))

cewl_search = cewl.intersection(search)
print('cewl * search: ' + str(len(cewl_search)))

uniq_dict = (dictionary.difference(cewl)).difference(search)
print('uniq dict: ' + str(len(uniq_dict)))
print(uniq_dict)

uniq_cewl = (cewl.difference(search)).difference(dictionary)
print('uniq cewl: ' + str(len(uniq_cewl)))
print(uniq_cewl)

uniq_search = (search.difference(dictionary)).difference(cewl)
print('uniq search: ' + str(len(uniq_search)))
print(uniq_search)

all_res = ((dictionary.union(cewl)).union(search).union(ne))
print(str(len(all_res)))

#
# ne_dict_uniq = ne.difference(dictionary)
# print(str(len(ne_dict_uniq)))
#
# ne_cewl_uniq = ne.difference(cewl)
# print(str(len(ne_cewl_uniq)))
#
# ne_search_uniq = ne.difference(search)
# print(str(len(ne_search_uniq)))
#
# ne_uniq = ((ne.difference(dictionary)).difference(cewl)).difference(search)
# print(str(len(ne_uniq)))
