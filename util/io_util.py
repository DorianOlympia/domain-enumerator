import csv
import os

OUTPUT_DIRECTORY = 'output'


def get_result_directory(name_core):
    return OUTPUT_DIRECTORY + '/' + name_core + '/'


def newline_separated_file_to_list(file_path):
    try:
        with open(file_path, mode='r') as result_file:
            return result_file.read().splitlines()
    except (IOError, TypeError):
        return [None]


def write_results(detected_ne, detected_acronyms, generated_acronyms, visited_urls, name_core):
    result_directory = get_result_directory(name_core)
    if not os.path.exists(result_directory):
        os.makedirs(result_directory)
    with open(result_directory + 'detected_nes', mode='w') as file:
        file.truncate(0)
        writer = csv.writer(file, delimiter='\n')
        writer.writerow(detected_ne)

    with open(result_directory + 'detected_acronyms', mode='w') as file:
        file.truncate(0)
        writer = csv.writer(file, delimiter='\n')
        writer.writerow(detected_acronyms.union(generated_acronyms))

    with open(result_directory + 'crawled_urls', mode='w') as file:
        file.truncate(0)
        writer = csv.writer(file, delimiter='\n')
        writer.writerow(visited_urls)


def normalise_dnsrecon_result(dnsrecon_result):
    result = set(x.split(' ')[3].lower() for x in dnsrecon_result)
    result = set(x.replace('www.', '') for x in result)
    return sorted(result)
