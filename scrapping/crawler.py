import logging
from queue import Queue

import requests
from bs4 import BeautifulSoup
from requests.exceptions import MissingSchema, ReadTimeout
from urllib3.exceptions import NewConnectionError, MaxRetryError

from enumeration.ner import extract_ne_custom, extract_all_acronyms_custom
from scrapping.extractor import extract_internal_refs, extract_content


def crawl_website_for_ne_custom(start_domain, root_domain, level=1, visited_sites_limit=50):
    urls_to_visit = Queue()
    urls_to_visit.put((start_domain, 1))
    visited_urls = set()
    extracted_ne = set()
    extracted_acronyms = set()
    detected_refs = set()
    while not urls_to_visit.empty() and len(visited_urls) < visited_sites_limit:
        site_to_visit, current_level = urls_to_visit.get()
        site_to_visit = _get_valid_site_url(site_to_visit)
        try:
            print(site_to_visit)
            html = requests.get(site_to_visit, timeout=5)
            visited_urls.add(site_to_visit)
            soup = BeautifulSoup(html.text)
            _detect_ne_and_acronyms_for_url(soup, extracted_ne, extracted_acronyms)
            extracted_refs = extract_internal_refs(soup, root_domain)
            if current_level < level:
                for ref in extracted_refs:
                    if ref not in visited_urls and ref not in detected_refs:
                        urls_to_visit.put((ref, current_level + 1))
            detected_refs.update(extracted_refs)
        except (NewConnectionError, MaxRetryError, requests.exceptions.ConnectionError, MissingSchema, ReadTimeout):
            logging.error('Connection refused : ' + site_to_visit)

    return extracted_ne, extracted_acronyms, visited_urls, detected_refs


def _get_valid_site_url(site):
    if _url_ok('http://www.' + site):
        return 'http://www.' + site
    elif _url_ok('http://' + site):
        return 'http://' + site
    elif _url_ok(site):
        return site
    else:
        return 'https://www.' + site


def crawl_urls_for_ne_custom(urls, root_domain):
    extracted_ne = set()
    extracted_acronyms = set()
    detected_refs = set()
    for url in urls:
        try:
            url = _get_valid_site_url(url)
            print(url)
            html = requests.get(url, timeout=5)
            soup = BeautifulSoup(html.text)
            detected_refs.update(extract_internal_refs(soup, root_domain))
            _detect_ne_and_acronyms_for_url(soup, extracted_ne, extracted_acronyms)
        except (NewConnectionError, MaxRetryError, requests.exceptions.ConnectionError, ReadTimeout):
            logging.error('Connection refused : ' + url)
    return extracted_ne, extracted_acronyms, urls, detected_refs


def _detect_ne_and_acronyms_for_url(soup, extracted_ne, extracted_acronyms):
    content = extract_content(soup)
    if content is not None:
        extracted_ne.update(extract_ne_custom(content))
        extracted_acronyms.update(extract_all_acronyms_custom(content))
    print("Num of detected entities:" + str(len(extracted_ne)))
    print("Num of detected acronyms:" + str(len(extracted_acronyms)))


def _url_ok(url):
    try:
        r = requests.head(url, timeout=5)
        return r.status_code == 200
    except requests.exceptions.ConnectionError:
        return False
