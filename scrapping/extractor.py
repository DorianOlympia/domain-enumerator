import re

WWW_PREFIX = "www."
HTTP_PREFIX = "http"
SCROLL_TO_ELEMENT_PATTERN = re.compile('.*#[^\s/]*$')


def extract_content(soup):
    return soup.getText()


def extract_internal_refs(soup, root_domain):
    """

    :param soup: web page data in soup format
    :param root_domain: domain being a root for search in format: www.xyz.com (without protocol and /)
    :return:
    """
    hrefs = set(filter(is_html_url,
                       (link.get('href') for link in soup.findAll('a', href=True))))
    return retrieve_normalized_internal_refs(hrefs, root_domain)


def retrieve_normalized_internal_refs(urls, root_domain):
    """
    :return: all urls in given domain without protocols
    """
    result = set()
    for url in urls:
        if WWW_PREFIX in url and root_domain in url:
            ulr_regex = re.search('www\.(.+?$)', url)
            if ulr_regex:
                result.add(ulr_regex.group(1))
        elif HTTP_PREFIX in url and root_domain in url:
            # Remove part up to www. and add to result
            url = re.sub('https', 'http', url)
            ulr_regex = re.search('http://(.+?$)', url)
            if ulr_regex:
                result.add(ulr_regex.group(1))
        elif root_domain in url:
            result.add(remove_consecutive_slashes(root_domain + '/' + url))
    return result


def remove_consecutive_slashes(str):
    return re.sub('/+', '/', str)


def is_html_url(url):
    return not SCROLL_TO_ELEMENT_PATTERN.match(url) and not any(
        word in url for word in ('.gif', '.jpg', 'jpeg', '.mp3', '.png', '.pdf'))
